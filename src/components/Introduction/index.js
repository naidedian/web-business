import React from "react";
import { Container, Row, Col } from "reactstrap";
import "./index.css";

const Introduction = props => {
  return (
    <section id="intro-section" className="intro-section dark-overlay">
      <h1 className="big-dev">DEVELOPER</h1>
      <Container>
        <Row>
          <Col className="text-center ">
            <h3 className="greetings-text">Hello! My name is</h3>
            <h1 className="text-light intro-title">Edian Reyes</h1>
            <div className="divider"></div>
            <div className="mt-5">
              <h3 className="intro-sub-text">Web Developer</h3>
              <div className="dot"></div>
              <h3 className="intro-sub-text">Entrepreneur</h3>
              <div className="dot"></div>
              <h3 className="intro-sub-text">Christian</h3>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default Introduction;
