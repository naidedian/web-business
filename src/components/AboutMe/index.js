import React from "react";
import "./index.css";
import { Container, Row, Col } from "reactstrap";
import aboutImg from "../../images/gray-laptop.jpg";

const AboutMe = props => {
  return (
    <section id="about" className="about-main text-center text-light">
      <Container>
        <Row>
          <Col md="6">
            <img src={aboutImg} alt="About" className="img-fluid" width="500" />
          </Col>
          <Col className="col-flex justify-content-center" md="6">
            <h2 className="p-2 section-title">About Me</h2>
            <h4 className="font-weight-bold sub-title">
              Get to know me a little better!
            </h4>
            <p id="about-me-desc" className="about-info pb-4 mb-0">
              Hello
              <span role="img" aria-labelledby="about-me-desc">
                👋
              </span>
              ! I am a professional Web Developer from the beautiful island of
              Puerto Rico. Been in the tech industry working as a developer for
              the past 5 years. From a clueless guy who didn't know how to code
              to a great diverse Web Developer. Developed really rich and
              beautiful websites for different types of industries and
              organizations. Really enjoy building solutions that covers a need
              for people businesses and companies.
            </p>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default AboutMe;
