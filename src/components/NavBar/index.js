import React, { useState } from "react";
import { Navbar as NavigationBar, Nav, NavItem, NavbarBrand } from "reactstrap";
import "./index.css";
import logoImg from "../../images/logo-white-letters.png";
import { Link as RouterLink } from "react-router-dom";
import { Link } from "react-scroll";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import SideBar from "../../containers/SideBar";

const NavBar = ({ className, color, style }) => {
  const [toggleSidebar, setToggle] = useState(false);
  return (
    <>
      <SideBar isOpen={toggleSidebar}></SideBar>
      <NavigationBar
        className={className}
        color={color}
        expand="md"
        style={style}
      >
        <NavbarBrand href="/">
          <img
            src={logoImg}
            width="150"
            className="img-fluid"
            alt="Edian Reyes Logo"
          />
        </NavbarBrand>
        <FontAwesomeIcon
          icon={faBars}
          size="3x"
          className="ml-auto hamburger"
          onClick={() => setToggle(!toggleSidebar)}
        ></FontAwesomeIcon>
        <Nav className="ml-auto" navbar>
          <NavItem>
            <Link
              href="#"
              to="top"
              smooth={true}
              // spy={true}
              activeClass="active-link"
              isDynamic={true}
              duration={800}
              className="nav-link link-item mr-3"
            >
              HOME
            </Link>
          </NavItem>
          <NavItem>
            <Link
              href="#"
              to="about"
              smooth={true}
              // spy={true}
              offset={-100}
              activeClass="active-link"
              isDynamic={true}
              duration={800}
              className="nav-link link-item mr-3"
            >
              ABOUT ME
            </Link>
          </NavItem>
          <NavItem>
            <Link
              href="#"
              to="services"
              smooth={true}
              // spy={true}
              offset={-100}
              activeClass="active-link"
              isDynamic={true}
              duration={800}
              className="nav-link link-item mr-3"
            >
              SERVICES
            </Link>
          </NavItem>
          <NavItem>
            <RouterLink
              to="/images"
              className="nav-link link-item mr-3 font-weight-bold"
            >
              IMAGES
            </RouterLink>
          </NavItem>
          {/* <NavItem>
          <Link
            href="#"
            to="projects"
            smooth={true}
            spy={true}
            offset={-100}
            activeClass="active-link"
            isDynamic={true}
            duration={800}
            className="nav-link link-item mr-3"
          >
            WORK
          </Link>
        </NavItem> */}
        </Nav>
      </NavigationBar>
    </>
  );
};

export default NavBar;
