import React from "react";
import { Card, CardTitle, CardText, CardBody, Container } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCode,
  faExpand,
  faCashRegister,
  faVideo,
  faUserFriends
} from "@fortawesome/free-solid-svg-icons";
import "./index.css";

const Services = () => {
  return (
    <section id="services" className="ptb-70">
      <div className="text-center">
        {/* <h2 className="p-2 section-title">Services</h2> */}
        <h4 className="font-weight-bold section-title mb-5">What I Do?</h4>
      </div>
      <Container fluid>
        <div className="ml-2 mr-2 row-flex justify-content-around">
          <Card className="mb-3 card-services">
            <FontAwesomeIcon
              icon={faCode}
              className="hidden-bg-icon"
              size="10x"
            ></FontAwesomeIcon>
            <CardBody>
              <CardTitle>
                <div className="m-3">
                  <FontAwesomeIcon
                    icon={faCode}
                    className="services-icon"
                    size="5x"
                  ></FontAwesomeIcon>
                </div>
                <h4>Web Development</h4>
              </CardTitle>
              {/* <CardSubtitle>Code Websites</CardSubtitle> */}
              <CardText>
                The goal is to develop rich, beautiful and dynamic websites. If
                you dream it and have a clear idea, it is doable. From
                <strong> idea</strong> to <strong>reality</strong>. That is the
                motto!
              </CardText>
            </CardBody>
          </Card>
          <Card className="mb-3 card-services">
            <FontAwesomeIcon
              icon={faCashRegister}
              className="hidden-bg-icon"
              size="10x"
            ></FontAwesomeIcon>
            <CardBody>
              <CardTitle>
                <div className="m-3">
                  <FontAwesomeIcon
                    icon={faCashRegister}
                    className="services-icon"
                    size="5x"
                  ></FontAwesomeIcon>
                </div>
                <h4>Ecommerce Development</h4>
              </CardTitle>
              {/* <CardSubtitle>Code Websites</CardSubtitle> */}
              <CardText>
                Do you have a product to sell?
                <br />
                We can build your ecommerce site on popular platforms like
                Shopify or Wordpress.
              </CardText>
            </CardBody>
          </Card>
          <Card className="mb-3 card-services">
            <FontAwesomeIcon
              icon={faExpand}
              className="hidden-bg-icon"
              size="10x"
            ></FontAwesomeIcon>
            <CardBody>
              <CardTitle>
                <div className="m-3">
                  <FontAwesomeIcon
                    icon={faExpand}
                    className="services-icon"
                    size="5x"
                  ></FontAwesomeIcon>
                </div>
                <h4>Responsive Design</h4>
              </CardTitle>
              {/* <CardSubtitle>Code Websites</CardSubtitle> */}
              <CardText>
                Mobile? Tablet? Laptop?
                <br /> Don't worry about it, your website will adjust to any
                device and will still look amazing.
              </CardText>
            </CardBody>
          </Card>
          {true && (
            <Card className="mb-3 card-services">
              <FontAwesomeIcon
                icon={faUserFriends}
                className="hidden-bg-icon"
                size="10x"
              ></FontAwesomeIcon>
              <CardBody>
                <CardTitle>
                  <div className="m-3">
                    {/* <img
                      src={faPhotoVideo}
                      className="services-icon"
                      style={{ width: "5em" }}
                      alt="TaeKwon-Do Icon"
                    ></img> */}
                    <FontAwesomeIcon
                      icon={faUserFriends}
                      className="services-icon"
                      size="5x"
                    ></FontAwesomeIcon>
                  </div>
                  <h4>TaeKwon-Do Online</h4>
                </CardTitle>
                {/* <CardSubtitle>Code Websites</CardSubtitle> */}
                <CardText>
                  We are offering private-virtual TaeKwon-Do classes that will
                  help your kids develop balance, flexibility, concentration and
                  cardiovascular resistance.
                </CardText>
              </CardBody>
            </Card>
          )}
        </div>
      </Container>
    </section>
  );
};

export default Services;
