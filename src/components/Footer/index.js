import React from "react";
import "./index.css";
import logoImg from "../../images/logo-white-letters.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebook, faTwitter } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

const Footer = props => {
  return (
    <footer>
      <div className="text-center mb-5">
        <a href="/" className="">
          <img
            src={logoImg}
            alt="Edian Reyes's Logo"
            width="200"
            className="img-fluid"
          />
        </a>
      </div>
      <div className="text-center text-white mt-4">
        <div>
          <h2>Let's Work Together</h2>
        </div>
        <div
          className={`row-flex justify-content-between bg-1024 mt-4 center-block pt-2 pb-2`}
        >
          <div>
            <p>
              <FontAwesomeIcon icon={faEnvelope} size="1x" />{" "}
              edian.reyes92@gmail.com
            </p>
          </div>
          <div>
            <a
              href="https://facebook.com/ediankreyes"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FontAwesomeIcon
                icon={faFacebook}
                className="social-icon mr-3"
                size="2x"
              ></FontAwesomeIcon>
            </a>
            <a
              href="https://twitter.com/edianreyes"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FontAwesomeIcon
                icon={faTwitter}
                className="social-icon ml-3"
                size="2x"
              ></FontAwesomeIcon>
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
