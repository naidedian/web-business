import React from "react";
import { Container, Row, Col } from "reactstrap";
import coffeeImg from "../../images/blurry-coffee.jpg";
import "./index.css";

const Portfolio = props => {
  return (
    <section id="projects" className="dark-gray-bg ptb-70">
      <Container fluid>
        <div className="text-center">
          <h4 className="white-text font-weight-bold section-title mb-5 ">
            Projects
          </h4>
        </div>
        <Row className="pt-4 pb-4 ml-3 mr-3">
          <Col md="4" className="mt-2 mb-2 project-item">
            <a href="#">
              <img src={coffeeImg} alt="coffee work" className="img-fluid" />
            </a>
          </Col>
          <Col md="4" className="mt-2 mb-2 project-item">
            <a href="#">
              <img src={coffeeImg} alt="coffee work" className="img-fluid" />
            </a>
          </Col>
          <Col md="4" className="mt-2 mb-2 project-item">
            <a href="#">
              <img src={coffeeImg} alt="coffee work" className="img-fluid" />
            </a>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default Portfolio;
