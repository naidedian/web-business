import React from "react";
import { Navbar as NavigationBar, Nav, NavItem } from "reactstrap";
import { Link } from "react-scroll";
import "./index.css";

const SideBar = ({ isOpen }) => {
  return (
    <div className={`sidebar-container ${isOpen ? "sidebar-open" : ""}`}>
      <NavigationBar className="sidebar-nav">
        <Nav className="d-block">
          <NavItem>
            <Link
              href="#"
              to="top"
              smooth={true}
              // spy={true}
              activeClass="active-link"
              isDynamic={true}
              duration={800}
              className="nav-link link-item mr-3"
            >
              HOME
            </Link>
          </NavItem>
          <NavItem>
            <Link
              href="#"
              to="about"
              smooth={true}
              // spy={true}
              offset={-100}
              activeClass="active-link"
              isDynamic={true}
              duration={800}
              className="nav-link link-item mr-3"
            >
              ABOUT ME
            </Link>
          </NavItem>
          <NavItem>
            <Link
              href="#"
              to="services"
              smooth={true}
              // spy={true}
              offset={-100}
              activeClass="active-link"
              isDynamic={true}
              duration={800}
              className="nav-link link-item mr-3"
            >
              SERVICES
            </Link>
          </NavItem>
          {/* <NavItem>
          <Link
            href="#"
            to="projects"
            smooth={true}
            spy={true}
            offset={-100}
            activeClass="active-link"
            isDynamic={true}
            duration={800}
            className="nav-link link-item mr-3"
          >
            WORK
          </Link>
        </NavItem> */}
        </Nav>
      </NavigationBar>
    </div>
  );
};

export default SideBar;
